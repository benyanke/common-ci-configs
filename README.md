# Common CI Configs

Sets of CI config files for including on other repos to provide centralization of common CI workflows.

## Usage

You are free to use these directly, but they are offered without support and may change at any time.

I suggest forking this repo for your own use if you find it valuable.

Every include exists as it's own directory. Each directory contains 2 files:
 
 - `include.yml` - this is the file that should actually be included
 - `readme.md` - this documents the functionality of the include

## List of Includes

Below is a listing of the includes in this repo, categorized by usage.

### Packaging

 - [Docker Build and Push](includes/packaging/docker-linux)
 - [Golang Build for Binary Project](includes/packaging/golang)
 - [Golang Build for Library w/o Binary](includes/packaging/golang-lib-only)
 - [Aptly Deb Uploader](includes/packaging/deb-upload)

### Common Building Blocks

 - [Semantic Version Tag Handlers](includes/common/tag-stages)


### Static Site Building

 - [Hugo](includes/static-website/build/hugo)

### Static Site Publishing

 - [Netlify CDN](includes/static-website/publish/netlify)
 - [Gitlab Pages](includes/static-website/publish/gitlab-pages)


### IAC

 - [Terraform Runner - with state storage in Gitlab Project](includes/terraform/gitlab-ci-state-terraform)
