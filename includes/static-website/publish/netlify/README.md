# Netlify Hugo Static Site Builder

This CI include provides a CI stage to publish static sites to the netlify CDN.

It provides jobs in the `deploy` stage.

Designed to be used with either the hugo build includes, which run in the `build` stage.

Publishes artifacts read from the `/public` folder.

## Example Usage

To use the netlify builder, add the following to your `.gitlab-ci.yml`:

```yml
---

stages:
  - build
  - deploy

include:

  # Include hugo builder (not required, can replace with another build stage, just an example)
  - project: 'benyanke/common-ci-configs'
    file: 'includes/static-website/build/hugo/include.yml'

  # Include netlify publisher
  - project: 'benyanke/common-ci-configs'
    file: 'includes/static-website/publish/netlify/include.yml'

```

## Expected Workflow

Does nothing on regular commits.

On the default branch, it publishes the artifacts found in `public` to the production site.

On commits on non-default branchs, it will create a branch environment site, with a URL based on the branch name. Each branch
will have it's own branch site. It's dynmically created by the branch name, look at the CI publish job output to find the
URL for your branch.

## Variables

All of the following variables must be set in your pipeline unless otherwise noted.

### NETLIFY_SITE_ID

Netlify's site ID (a UUID) to publish the site to.

Find this by logging into your netlify account, opening the site's entry, then
going to "Site Settings" -> "Site details" -> "Site ID".

Example value: `6cf9faa6-b842-11ec-b909-0242ac120002`

### NETLIFY_AUTH_TOKEN

API token for publish to netlify. Get yours at: https://app.netlify.com/user/applications/personal

Example value: `KXpzjCnkZKXXO3vf56N8v6TqCEbThE9JwVUYzG6Y`
