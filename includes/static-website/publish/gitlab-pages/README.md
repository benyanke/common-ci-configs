# Gitlab Pages Site Publisher

This CI include provides a CI stage to publish static sites to gitlab pages.

As Gitlab pages does not support multiple environments/deployments, this include
exclusively operates on commits to the default branch.

All other commits are ignored.

Publishes artifacts read from the `/public` folder.

## Example Usage

To use the gitlab pages	 builder, add the following to your `.gitlab-ci.yml`:

```yml
---

stages:
  - build
  - deploy

include:

  # Include hugo builder (not required, can replace with another build stage, just an example)
  - project: 'benyanke/common-ci-configs'
    file: 'includes/static-website/build/hugo/include.yml'

  # Include gitlab pages publisher
  - project: 'benyanke/common-ci-configs'
    file: 'includes/static-website/publish/gitlab-pages/include.yml'

```

## Expected Workflow

Does nothing on regular commits.

On the default branch, it publishes the artifacts found in `public` to the gitlab pages site.

## Variables

No configurable options.
