# Hugo Static Site Builder

This CI include provides a CI stage to build Hugo static sites.

It provides jobs in the `build` stage.

Designed to be used with either the netlify publish or gitlab pages publish includes, which run in the `publish` stage.

Outputs artifacts to the `/public` folder for publishing.

## Example Usage

To use the hugo builder, add the following to your `.gitlab-ci.yml`:

```yml
---

stages:
  - build
  - deploy

include:

  # Include hugo builder
  - project: 'benyanke/common-ci-configs'
    file: 'includes/static-website/build/hugo/include.yml'

  # Include netlify publisher builder (not required, can replace with another publish stage, just an example)
  - project: 'benyanke/common-ci-configs'
    file: 'includes/static-website/publish/netlify/include.yml'

```

## Expected Workflow

Does nothing on regular commits.

On the default branch, it builds hugo with the base url of `$SITE_BASE_URL`
.
On the non-default branches, it builds hugo with the base url of `/`.

## Variables

All of the following variables must be set in your pipeline unless otherwise noted.

### SITE_BASE_URL

Base URL to use for default branch builds.

Example value: `https://site.example.com`

### HUGO_BUILD_CONTAINER_IMAGE (optional)

Container image to use for hugo build

Default value: `registry.gitlab.com/pages/hugo/hugo_extended`

### HUGO_BUILD_CONTAINER_TAG (optional)

Container tag to use for hugo build

Default value: `latest`

### HUGO_BUILD_PAGEFIND_VERSION (optional)

Version of pagefind to use, if pagefind is used on the current project.

Default value: see include.yml for the current latest version

### HUGO_BUILD_PAGEFIND_SHA256 (optional)

sha256 hash of the pagefind binary - the pipeline validates this before running it, since
it's downloaded on every deployment. This ensures we're safely downloading the expected binary.

Default value: see include.yml for the current latest version
