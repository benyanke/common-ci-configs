# All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry

# Ensure the following CI env vars are set in the gitlab gui 
#   - SITE_BASE_URL is set, this is the base URL to build hugo with

# This CI does several things:
#   - Builds hugo on every commit
#   - Uses SITE_BASE_URL for the default branch, for deployment to prod
#   - Uses "/" as the base url for everything else, for deployment to testing environments (optionally)

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  # broke with 0.131.0
  # HUGO_BUILD_CONTAINER_TAG: "latest"
  HUGO_BUILD_CONTAINER_TAG: "0.123.0"
  HUGO_BUILD_CONTAINER_IMAGE: "registry.gitlab.com/pages/hugo/hugo_extended"

  HUGO_BUILD_PAGEFIND_VERSION: "v0.9.2"
  HUGO_BUILD_PAGEFIND_SHA256: "19c4cd769d863771071ee9ff8956f54cbadee69cb773786375b158dae96b694d"

stages:
  - build

###############
# CI stages
###############

# Builds the hugo site, returns the static HTML files to artifact into the deployment stages

# prod build
hugo_build_prod:
  extends: .base_hugo_build
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - HUGO_ENV=production hugo --baseURL "${SITE_BASE_URL}"

    # Generate static site search index if needed
    - |
        if [[ -f "pagefind.yml" ]] ; then
           wget -qO- https://github.com/CloudCannon/pagefind/releases/download/${HUGO_BUILD_PAGEFIND_VERSION}/pagefind-${HUGO_BUILD_PAGEFIND_VERSION}-x86_64-unknown-linux-musl.tar.gz | tar -zxv -C /usr/local/bin/ &&
           printf "${HUGO_BUILD_PAGEFIND_SHA256}  /usr/local/bin/pagefind" | sha256sum -c &&
           /usr/local/bin/pagefind;
        fi

# non-prod build, setting base URL to / to handle draft environments correctly
hugo_build_nonprod:
  extends: .base_hugo_build
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
  script:
    - hugo --baseURL "/"

    # Generate static site search index if needed
    - |
        if [[ -f "pagefind.yml" ]] ; then
           wget -qO- https://github.com/CloudCannon/pagefind/releases/download/${HUGO_BUILD_PAGEFIND_VERSION}/pagefind-${HUGO_BUILD_PAGEFIND_VERSION}-x86_64-unknown-linux-musl.tar.gz | tar -zxv -C /usr/local/bin/ &&
           printf "${HUGO_BUILD_PAGEFIND_SHA256}  /usr/local/bin/pagefind" | sha256sum -c &&
           /usr/local/bin/pagefind;
        fi


###############
# Inherit base configs
###############

# these are inherited by the stages above, and provide a default point from which keys can be overridden above - they themselves are not CI stages and aren't
# run themselves

# Base hugo build stage, inherited by the prod and non-prod deploy stages with stage-specific overrides
.base_hugo_build:
  stage: build
  image: $HUGO_BUILD_CONTAINER_IMAGE:$HUGO_BUILD_CONTAINER_TAG
  retry: 1
  before_script:
    # Install deps from NPM if needed
    - |
        if [[ -f "package.json" ]] ; then
          apk add npm &&
          npm ci ;
        fi
  after_script:
    # prints the listing of all files for later debugging
    - find public
  variables:
    # mask this to ensure that hugo never is able to access the real key
    # used in the publish stages
    NETLIFY_AUTH_TOKEN: "masked"
  artifacts:
    expire_in: 3 hours
    paths:
    - public
