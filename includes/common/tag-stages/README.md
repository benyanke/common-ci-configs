# Tag Handling Stubs

This CI include provides a common ci stage run rules for semantic version based workflows, which can be
utilized via the `extends` directive.

## Example Usage


```yml
---

include:

  # Include tag-stages rules
  - project: 'benyanke/common-ci-configs'
    file: 'includes/common/tag-stages/include.yml'
```


```yml
---

include:

  # Include tag-stages rules
  - project: 'benyanke/common-ci-configs'
    file: 'includes/common/tag-stages/include.yml'

# Push all non-tag commits to test repo
push_test_repo:
  stage: deploy
  extends:
    - .tagsNone
  script:
    - ./pushToRepo.py --stability="test"

# Push release candidates to the staging repo
push_staging_repo:
  stage: deploy
  extends:
    - .tagsPreRelease
  script:
    - ./pushToRepo.py --stability="staging"

# Push final releases to the production repo
push_prod_repo:
  stage: deploy
  extends:
    - .tagsRelease
  script:
    - ./pushToRepo.py --stability="production"

```

## Provided Includes

Below is a listing of all the includes provided here. These are human readable guides,
see `include.yml` for the exact regex in each.

### .tagsNone

Only runs when no git tags are on the current pipeline.

### .tagsPreRelease

Only runs when the git tag on the pipeline that match the pattern `vX.X.X-rcX`
for the purpose of capturing release candidate / pre-release tags.

### .tagsRelease

Only runs when the git tag on the pipeline that match the pattern `vX.X.X`
for the purpose of capturing final release tags.

### .tagsValid

Only runs when the git tag on the pipeline that match the pattern `vX.X.X`
or match `vX.X.X-rcX` for the purpose of capturing all semver-style tags
that might be used as part of a release process.

### .tagsInvalid

Only runs when there is a git tag on the pipeline, and it doesn't match
`vX.X.X` or `vX.X.X-rcX`. This would typically be used as part of error handling
if you want to only allow semver tags on your repo.
