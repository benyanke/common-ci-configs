# Golang Project Builder for Libraries

This CI include provides a common workflow for building golang libraries that don't
require a released binary.

For golang projects primarily released as a docker container, use the docker builder.

For golang projects with a released binary, use the standard golang builder.

## Example Usage

To use this include without any image testing prior to push, set your 
`.gitlab-ci.yml` to the following:

```yml
---

include:

  # Include golang builder
  - project: 'benyanke/common-ci-configs'
    file: 'includes/packaging/golang-lib-only/include.yml'

```

## Expected Workflow

Commit on any branch and the project will be tested and linted. This
provides instant feedback if a commit breaks the build.

When you're ready to prepare for a release, take a commit and tag it with the 
version you intend to release, appended with '-rcX' where X is an incrementing 
integer. If you are intending to prepare for release v2.0.0, you should first tag 
'v2.0.0-rc1' so that you can test. Make more release candidates as needed.

When all the issues have been worked out, make a git tag for the release 
itself. Following our example above, you would now make git tag 'v2.0.0'. It 
then makes a gitlab release from the tag too.

## Variables

The following variables can be overridden in child pipelines. All of them have
sane defaults so none of them need to be overriden typically.

### GOLANG_VERSION

Set the golang version for building.