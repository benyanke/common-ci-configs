# Deb Uploader

This CI include provides a deb uploader for uploading DEB packages to an
Aptly repo.

It provides jobs in the `upload` stage.

## Example Usage

To use the deb uploader, add the following to your `.gitlab-ci.yml`:

```yml
---

include:

  # Include deb uploader
  - project: 'benyanke/common-ci-configs'
    file: 'includes/packaging/deb-upload/include.yml'

```

## Expected Workflow

Does nothing on regular commits.

On release candidate tags (`vX.X.X-rcX`), the deb will be pushed to the repo
indicated in `$APTLY_REPO_RC`.

On release candidate tags (`vX.X.X`), the deb will be pushed to the repo
indicated in `$APTLY_REPO_PROD`.

## Variables

All of the following variables must be set in your pipeline unless otherwise noted.

### APTLY_URL

Root URL to the Aptly instance.

Example value: `https://aptly.example.com`

### APTLY_DIGEST_AUTH

Digest auth string for authenticating. NOTE: aptly does not provide
authentication itself. This assumes your reverse proxy is using
a digest auth scheme in front of aptly.

Example value: `johndoe:default:29c5d3e4ea28a7d21ae467cd6b371f83`

### APTLY_REPO_PROD

Name of the repo to push releases to.

Example value: `repo-prod`

### APTLY_REPO_RC

Name of the repo to push release candidates to.

Example value: `repo-unstable`


### APTLY_STORAGE_PREFIX

This is the aptly storage location for repo publishing.

Does not typically need to be set unless overriding the default.

Default value: `s3:apt-yanke-io`

### APTLY_DISTRO

This is the aptly distro location for repo publishing.

Does not typically need to be set unless overriding the default.

Default value: `focal`

### APTLY_REPO_PREFIX_PROD

Publishing path in the bucket for the prod repo.

Default value: `repo/prod`

### APTLY_REPO_PREFIX_RC

Publishing path in the bucket for the release candidate repo.

Default value: `repo/dev`

