# Golang Project Builder

This CI include provides a common workflow for building golang projects
outside a docker image.

For golang projects primarily released as a docker container, use the docker builder.

## Example Usage

To use this include without any image testing prior to push, set your 
`.gitlab-ci.yml` to the following:

```yml
---

include:

  # Include golang builder
  - project: 'benyanke/common-ci-configs'
    file: 'includes/packaging/golang/include.yml'

```

## Expected Workflow

Commit on any branch and the project will be built. This provides instant 
feedback if a commit breaks the build, and provides a way for developers to 
test against a specific commit, or the latest state of a particular branch.

When you're ready to prepare for a release, take a commit and tag it with the 
version you intend to release, appended with '-rcX' where X is an incrementing 
integer. If you are intending to prepare for release v2.0.0, you should first tag 
'v2.0.0-rc1' so that you can test. Make more release candidates as needed.

When all the issues have been worked out, make a git tag for the release 
itself. Following our example above, you would now make git tag 'v2.0.0'. It 
then makes a gitlab release from the tag too.

## Variables

The following variables can be overridden in child pipelines. All of them have
sane defaults so none of them need to be overriden typically.

### GO_BUILD_ARCH_LIST and GO_BUILD_OS_LIST

Each of these are a space-seperated list of architectures and OSes to build 
binaries for. The two are combined and every valid combination is build. Must
be valid according to the command `go tool dist list`, invalid combinations are
silently skipped.

For example, given the following config:

 - GO_BUILD_ARCH_LIST: 'amd64 arm64'
 - GO_BUILD_OS_LIST: 'linux darwin'

The following binaries will be built:

 - linux amd64
 - linux arm64
 - darwin amd64
 - darwin arm64

### GOLANG_VERSION

Set the golang version for building.

### GO_BUILD_ARGS

Extra build arguments to pass to `go build`.
