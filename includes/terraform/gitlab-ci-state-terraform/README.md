# Terraform in Gitlab CI - with Gitlab State Storage

This CI runs terraform, using the `gitlab-terraform` wrapper, making use of gitlab
project terraform state storage.

## Example Usage

To use the netlify builder, add the following to your `.gitlab-ci.yml`:

```yml
---

stages:
    - plan
    - deploy
    - destroy

include:

  # terraform runner with gitlab state storage
  - project: 'benyanke/common-ci-configs'
    file: 'includes/terraform/gitlab-ci-state-terraform/include.yml'

```

## Expected Workflow

On the default branch, it applies the terraform.

On commits on non-default branchs, it runs a terraform plan, but not apply. This allows
you to review plan diffs on merge requests.

## Variables

Environment variables that can be set. All are optional.

### SSH_PRIVATE_KEY_B64

This is optional, set to a base-64 encoded SSH private key.

Will be unpacked into file `key` in the terraform working directory, can be used for
post provisioning tasks.

### TF_CONTAINER_IMAGE

Container image to use for terraform runner.

Default value: `registry.gitlab.com/gitlab-org/terraform-images/stable`

### TF_CONTAINER_TAG

Container tag to use for terraform runner.

Default value: `latest`

### TF_ADD_SLEEPS

Set to `true` to add sleep statements throughout execution. Some providers have API limits, this is useful
for slowing down terraform execution.

Default value: `false`

